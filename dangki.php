<?php
    $gioiTinh = [
        "0" => "Nam",
        "1" => "Nữ"
    ];

    $khoa = [
        "-1" => "Trống",
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    ];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .container {
        display: flex;
        height: 100vh;
        justify-content: center;
    }

    .wrapper {
        display: flex;
        justify-content: center;
        flex-direction: column;
    }

    form {
        width: 500px;
        border: 1px solid rgb(188, 208, 246);
        padding: 30px;
    }

    p {
        background: lightgray;
        padding: 8px;
    }

    .name {
        margin: 12px 0px;
        display: flex;
    }

    .gtinh {
        margin-top: 12px;

    }

    .radio,
    .khoa {
        margin-left: 20px;
        border: 1px solid rgb(87, 137, 245);
    }

    .lab {
        background: cornflowerblue;
        padding: 8px 10px;
        border: 2px solid blue;
        width: 25%;
        display: block;
        color: white;
    }

    .inp {
        margin-left: 20px;
        width: 60%;
        border: 1px solid rgb(87, 137, 245);
    }

    button {
        background: rgb(4, 129, 44);
        padding: 8px 10px;
        border: solid 2px rgb(154, 154, 232);
        width: 25%;
        display: block;
        color: white;
        border-radius: 6px;
    }

    .submit {
        display: flex;
        justify-content: center;
    }

    .gioitinh-wrapper>div {
        margin-left: 20px;
        margin-top: 8px;
    }
    </style>
</head>

<body>
    <div class="container">
        <div class="wrapper">

            <div>
                <form action="">
                    <div class="name">
                        <label class="lab">Họ và tên</label>
                        <input class="inp" type="text" />
                    </div>
                    <div class="name gioitinh-wrapper">
                        <label class="lab">Giới tính</label>
                        <div>
                            <?php
                foreach($gioiTinh as $key => $item) {
             ?>
                            <label for="<?=$key?>" class="gtinh"><?=$item?></label>
                            <input type="radio" name="gtinh" value="<?=$item?>">
                            <?php 
                }
             ?>
                        </div>
                    </div>
                    <div class="name">
                        <label class="lab">Phân Khoa</label>
                        <select name="khoa" id="khoa" class="khoa">
                            <?php 
                    foreach($khoa as $key => $item) {
                ?>
                            <option value="<?=$key?>"><?=$item?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="submit">
                        <button>Đăng kí</button>
                    </div>
                </form>
            </div>

        </div>

    </div>
    </div>
</body>

</html>